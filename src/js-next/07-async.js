/**
 * Fix the async retreive method, doing proper error handling
 * in case fetch does not return a user
 */
exports.User = class {

    constructor(url) {
        this.url = url;
    }

    async retreive() {
        const user = await fetch (this.url);
        
        if (!user) {
            throw new Error('Error!');
        }

        return user;
    }
};

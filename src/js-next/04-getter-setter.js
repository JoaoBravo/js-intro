/**
 * Return an object with a login property,
 * accessible only through a getter and setter
 */
exports.buildLogin = function() {

    let login = ' ';
    
    return {
        get login() {
            return login;
        },
        set login(value) {
            login = value;
        }
    };
};

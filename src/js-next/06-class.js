/**
 * Create a Prefixer class with constructor and a prefixArray method
 */
exports.Prefixer = class Prefixer {
    constructor(prefix) {
        this.prefix = prefix;
    }
    prefixArray(array) {
        return array.map( (i) => this.prefix + i);
    }
};

/**
 * Create a PrefixerSuffixer class which extends from the Prefixer class
 * but adds a sufixArray method
 */
exports.PrefixerSufixer = class PrefixerSuffixer extends this.Prefixer {

    constructor(prefix,sufix) {
        super(prefix);
        this.sufix = sufix;
    }

    sufixArray(array) {
        return array.map ( (i) => i + this.sufix);
    }
};
